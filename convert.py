def m2s(minutes):
    """Convert the number of minutes to seconds and returns it."""
    return minutes * 60

def h2m(hours):
    """Convert the number of hours to minutes and returns it."""
    return hours * 60

def d2s(days):
    """Convert the number of days to seconds and returns it."""
    return m2s(h2m(days * 24))

def d2m(days):
    """Convert the number of days to minutes and returns it."""
    return h2m(days * 24)

def d2h(days):
    """Convert the number of days to hours( and returns it."""
    return days * 24
